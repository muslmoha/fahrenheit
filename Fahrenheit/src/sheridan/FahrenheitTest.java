//By Mohammed Musleh
//Sheridan log ing muslmoha
//Student ID 991508115
package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular() {
		assertTrue("Please enter valid temp", (Fahrenheit.fromCelsius(1) == 34));
	}
	
	@Test(expected = AssertionError.class)
	public void testFromCelsiusException() {
		assertTrue("Please enter valid temp", (Fahrenheit.fromCelsius(-901) == -1588));
	}
	
	@Test
	public void testFromCelsiusBoundaryIn() {
		assertTrue("Please enter valid temp", (Fahrenheit.fromCelsius(-400) == -688));
	}
	
	@Test
	public void testFromCelsiusBoundaryOut() {
		assertFalse("Please enter valid temp", (Fahrenheit.fromCelsius(-401) == -690));
	}

}
